package com.example.practica03_listviewalumnos;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    AdapterAlumno adapterA;
    ListView sp1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ArrayList<alumnoItem> list = new ArrayList<>();
        list.add(new alumnoItem(getString(R.string.nombre1), getString(R.string.mat1), R.drawable.a1));
        list.add(new alumnoItem(getString(R.string.nombre2), getString(R.string.mat2), R.drawable.a2));
        list.add(new alumnoItem(getString(R.string.nombre3), getString(R.string.mat3), R.drawable.a3));
        list.add(new alumnoItem(getString(R.string.nombre4), getString(R.string.mat4), R.drawable.a4));
        list.add(new alumnoItem(getString(R.string.nombre5), getString(R.string.mat5), R.drawable.a5));
        list.add(new alumnoItem(getString(R.string.nombre6), getString(R.string.mat6), R.drawable.a6));
        list.add(new alumnoItem(getString(R.string.nombre7), getString(R.string.mat7), R.drawable.a7));
        list.add(new alumnoItem(getString(R.string.nombre8), getString(R.string.mat8), R.drawable.a8));
        list.add(new alumnoItem(getString(R.string.nombre9), getString(R.string.mat9), R.drawable.a9));
        list.add(new alumnoItem(getString(R.string.nombre10), getString(R.string.mat10), R.drawable.a10));
        list.add(new alumnoItem(getString(R.string.nombre11), getString(R.string.mat11), R.drawable.a11));
        list.add(new alumnoItem(getString(R.string.nombre12), getString(R.string.mat12), R.drawable.a12));
        list.add(new alumnoItem(getString(R.string.nombre13), getString(R.string.mat13), R.drawable.a13));
        list.add(new alumnoItem(getString(R.string.nombre14), getString(R.string.mat14), R.drawable.a14));
        list.add(new alumnoItem(getString(R.string.nombre15), getString(R.string.mat15), R.drawable.a15));
        list.add(new alumnoItem(getString(R.string.nombre16), getString(R.string.mat16), R.drawable.a16));
        list.add(new alumnoItem(getString(R.string.nombre17), getString(R.string.mat17), R.drawable.a17));
        list.add(new alumnoItem(getString(R.string.nombre18), getString(R.string.mat18), R.drawable.a18));
        list.add(new alumnoItem(getString(R.string.nombre19), getString(R.string.mat19), R.drawable.a19));
        list.add(new alumnoItem(getString(R.string.nombre20), getString(R.string.mat20), R.drawable.a20));
        list.add(new alumnoItem(getString(R.string.nombre21), getString(R.string.mat21), R.drawable.a21));
        list.add(new alumnoItem(getString(R.string.nombre22), getString(R.string.mat22), R.drawable.a22));
        list.add(new alumnoItem(getString(R.string.nombre23), getString(R.string.mat23), R.drawable.a23));
        list.add(new alumnoItem(getString(R.string.nombre24), getString(R.string.mat24), R.drawable.a24));
        list.add(new alumnoItem(getString(R.string.nombre25), getString(R.string.mat25), R.drawable.a25));
        list.add(new alumnoItem(getString(R.string.nombre26), getString(R.string.mat26), R.drawable.a26));
        list.add(new alumnoItem(getString(R.string.nombre27), getString(R.string.mat27), R.drawable.a27));
        list.add(new alumnoItem(getString(R.string.nombre28), getString(R.string.mat28), R.drawable.a28));
        list.add(new alumnoItem(getString(R.string.nombre29), getString(R.string.mat29), R.drawable.a29));
        list.add(new alumnoItem(getString(R.string.nombre30), getString(R.string.mat30), R.drawable.a30));
        list.add(new alumnoItem(getString(R.string.nombre31), getString(R.string.mat31), R.drawable.a31));
        list.add(new alumnoItem(getString(R.string.nombre32), getString(R.string.mat32), R.drawable.a32));

        //AdapterAlumno adapter = new AdapterAlumno(this, R.layout.item_alumno, R.id.lblMatricula, list);
        adapterA = new AdapterAlumno(this, R.layout.item_alumno, R.id.lblMatricula, list);


        sp1=findViewById(R.id.spinner1);
        sp1.setAdapter(adapterA);
        sp1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(adapterView.getContext(),getString(R.string.msgAlumno).toString() +" "+((alumnoItem)adapterView.getItemAtPosition(i)).getTextNombre(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    //Si no se encuentra devuelve una notificacion
    private void resetAdapterData() {
        if (adapterA != null) {
            adapterA.resetData();
            adapterA.notifyDataSetChanged();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.searchview, menu);
        MenuItem menuItem = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) menuItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                resetAdapterData();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapterA.filter(newText);
                return true;
            }
        });

        return true;
    }


}
