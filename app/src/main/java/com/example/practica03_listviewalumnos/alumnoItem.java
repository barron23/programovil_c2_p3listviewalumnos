package com.example.practica03_listviewalumnos;

public class alumnoItem {
    private String textNombre;
    private String textMatricula;
    private Integer imageId;

    //Declaracion del constructor
    public alumnoItem(String text, String text2, Integer imageId){
        this.textNombre = text;
        this.textMatricula = text2;
        this.imageId =imageId;
    }

    public String getTextNombre() {
        return textNombre;
    }

    public void setTextNombre(String text2) {
        this.textNombre = text2;
    }

    public String getTextMatricula(){
        return textMatricula;
    }

    public void setTextMatricula(String text){
        this.textMatricula = text;
    }

    public Integer getImageId(){
        return imageId;
    }

    public void setImageId(Integer imageId){
        this.imageId = imageId;
    }
}
