package com.example.practica03_listviewalumnos;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class AdapterAlumno extends ArrayAdapter<alumnoItem> {
    int groupId;
    Activity Context;
    //No filtro
    //ArrayList<alumnoItem> list;
    ArrayList<alumnoItem> list;
    ArrayList<alumnoItem> listaSearch;
    LayoutInflater inflater;
    /*public AdapterAlumno(Activity Context,int groupId,int id,ArrayList<alumnoItem>list){
        super(Context,id,list);
        this.list = list;
        inflater =(LayoutInflater)Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.groupId = groupId;*/

    public AdapterAlumno(Activity Context, int groupId, int id, ArrayList<alumnoItem> list) {
        super(Context, id, list);
        this.list = new ArrayList<>(list);
        this.listaSearch = new ArrayList<>(list);
        inflater = (LayoutInflater) Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.groupId = groupId;
    }

    /*
    public View getView(int posicion, View convertView, ViewGroup parent){
        View itemView = inflater.inflate(groupId,parent,false);
        ImageView imagen = (ImageView) itemView.findViewById(R.id.imgMatricula);
        imagen.setImageResource(list.get(posicion).getImageId());
        TextView textMatricula = (TextView) itemView.findViewById(R.id.lblMatricula);
        textMatricula.setText(list.get(posicion).getTextMatricula());
        TextView textNombre = (TextView)itemView.findViewById(R.id.lblNombre);
        textNombre.setText(list.get(posicion).getTextNombre());
        return itemView;
    }
     */

    //Contraer a la lista principal
    public void resetData() {
        listaSearch = new ArrayList<>(list);
    }

    //Encontrar posicion en lista
    public alumnoItem getItem(int position) {
        return listaSearch.get(position);
    }

    //Obtener el tamaño de la lista filtrada
    @Override
    public int getCount() {
        return listaSearch.size();
    }

    //Encontrar alumno con la busqueda
    public void filter(String query) {
        listaSearch.clear();
        if (query.isEmpty()) {
            //Buscar en la lista
            listaSearch.addAll(list);
        } else {
            //convertir a minusculas
            query = query.toLowerCase();
            //Busca por nombre o matricula si lo que se escribio se encuentra en algun campo
            for (alumnoItem item : list) {
                if (item.getTextMatricula().toLowerCase().contains(query) ||
                        item.getTextNombre().toLowerCase().contains(query)) {
                    //se muestra lo encontrado
                    listaSearch.add(item);
                }
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = convertView;

        if (itemView == null) {
            itemView = inflater.inflate(groupId, parent, false);
        }

        //Muestra lo encontrado
        ImageView imageView = itemView.findViewById(R.id.imgMatricula);
        TextView txtMatricula = itemView.findViewById(R.id.lblMatricula);
        TextView txtNombre = itemView.findViewById(R.id.lblNombre);

        //Otorga la posicion en un item
        alumnoItem currentItem = listaSearch.get(position);

        //Muestra el alumno encontrado
        imageView.setImageResource(currentItem.getImageId());
        txtMatricula.setText(currentItem.getTextMatricula());
        txtNombre.setText(currentItem.getTextNombre());

        return itemView;
    }




    /*
    public View getDropDownView(int posicion,View convertView,ViewGroup parent){
        return getView(posicion,convertView,parent);
    }*/

}

